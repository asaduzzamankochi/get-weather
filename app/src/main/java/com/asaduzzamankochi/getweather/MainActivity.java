package com.asaduzzamankochi.getweather;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;


public class MainActivity extends ActionBarActivity {

//    Typeface weatherFont;

    private TextView txtCity;
    private TextView txtUpdated;
    private TextView txtDetails;
    private TextView txtCurrentTemperature;
    private TextView txtWarningMessage;
    private ImageView weatherIcon;

    private String currentCity;
    private String updateOn;
    private String detailsField;
    private String currentTemp;

    private String city;
    private double latitude;
    private double longitude;

    private boolean internetCheck;

    SharedPreferences prefs;

    int id = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtCity = (TextView) findViewById(R.id.city_field);
        txtUpdated = (TextView) findViewById(R.id.updated_field);
        txtDetails = (TextView) findViewById(R.id.details_field);
        txtCurrentTemperature = (TextView) findViewById(R.id.current_temperature_field);
        txtWarningMessage = (TextView) findViewById(R.id.warning_message);
        weatherIcon = (ImageView) findViewById(R.id.weather_icon);
//        weatherFont = Typeface.createFromAsset(getAssets(), "fonts/weather.ttf");
//        txtCity.setText("City");
//        txtUpdated.setText("Updaff");
//        txtDetails.setText("dsdsd");
//        txtCurrentTemperature.setText("Temperature");

        prefs = getSharedPreferences("myPref", MODE_PRIVATE);

//        weatherIcon.setTypeface(weatherFont);


        currentWeather();
    }


    private void currentWeather() {

        if (prefs.contains("city")) {
            city = prefs.getString("city", "");
            if(city.contains(",")){

            }
            else {
                city = city+",bd";

            }
        } else {
            city = "Dhaka,bd";
        }

        internetCheck = isInternetAvailable(this);
        if (internetCheck) {
            //Internet available
            if (id==0){
                updateWeatherDataByCity();
            }
            else if (id==1){
                updateWeatherDataByCoordinate();
            }

        } else {
            //No Internet available
            txtCity.setVisibility(View.GONE);
            txtUpdated.setVisibility(View.GONE);
            txtDetails.setVisibility(View.GONE);
            txtCurrentTemperature.setVisibility(View.GONE);
            weatherIcon.setVisibility(View.GONE);
            txtWarningMessage.setVisibility(View.VISIBLE);
            txtWarningMessage.setText("Internet Not Available !!!");
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setTitle(getString(R.string.internet_not_available));
            alertDialogBuilder.setMessage(getString(R.string.connect));
            alertDialogBuilder.setPositiveButton(getString(R.string.ok), null);
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();

        }

    }


    private void updateWeatherDataByCity() {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://api.openweathermap.org/data/2.5/weather?q=" + city + "&units=metric",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            txtCity.setVisibility(View.VISIBLE);
                            txtUpdated.setVisibility(View.VISIBLE);
                            txtDetails.setVisibility(View.VISIBLE);
                            txtCurrentTemperature.setVisibility(View.VISIBLE);
                            weatherIcon.setVisibility(View.VISIBLE);
                            txtWarningMessage.setVisibility(View.GONE);
                            txtCity.setText("City");
                            txtUpdated.setText("Updaff");
                            txtDetails.setText("dsdsd");
                            txtCurrentTemperature.setText("Temperature");

                            JSONObject obj = new JSONObject(response);
                            currentCity = obj.getString("name");//.toUpperCase(Locale.US);
                            currentCity = currentCity + ", " + obj.getJSONObject("sys").getString("country");
                            txtCity.setText(currentCity);

                            JSONObject details = obj.getJSONArray("weather").getJSONObject(0);
                            JSONObject main = obj.getJSONObject("main");
                            txtDetails.setText(
                                    details.getString("description").toUpperCase(Locale.US) +
                                            "\n" + "Humidity: " + main.getString("humidity") + "%" +
                                            "\n" + "Pressure: " + main.getString("pressure") + " hPa");

                            txtCurrentTemperature.setText(
                                    String.format("%.2f", main.getDouble("temp")) + (char) 0x00B0 + "C");

                            DateFormat df = DateFormat.getDateTimeInstance();
                            String updatedOn = df.format(new Date(obj.getLong("dt") * 1000));
                            txtUpdated.setText("Last update: " + updatedOn);

                            setWeatherIcon(details.getInt("id"),
                                    obj.getJSONObject("sys").getLong("sunrise") * 1000,
                                    obj.getJSONObject("sys").getLong("sunset") * 1000);


                        } catch (JSONException e) {
                            Toast.makeText(getApplicationContext(), getString(R.string.place_not_found),
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                    }
                });
        ServiceHelper.getInstance().addToRequestQueue(stringRequest);
    }

    private void updateWeatherDataByCoordinate() {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://api.openweathermap.org/data/2.5/weather?lat=" + latitude + "&lon=" + longitude + "&units=metric",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            txtCity.setVisibility(View.VISIBLE);
                            txtUpdated.setVisibility(View.VISIBLE);
                            txtDetails.setVisibility(View.VISIBLE);
                            txtCurrentTemperature.setVisibility(View.VISIBLE);
                            weatherIcon.setVisibility(View.VISIBLE);
                            txtWarningMessage.setVisibility(View.GONE);


                            JSONObject obj = new JSONObject(response);
                            currentCity = obj.getString("name");//.toUpperCase(Locale.US);

                            SharedPreferences.Editor editor = prefs.edit();
                            editor.putString("city", currentCity);
                            editor.commit();

                            currentCity = currentCity + ", " + obj.getJSONObject("sys").getString("country");
                            txtCity.setText(currentCity);

                            JSONObject details = obj.getJSONArray("weather").getJSONObject(0);
                            JSONObject main = obj.getJSONObject("main");
                            txtDetails.setText(
                                    details.getString("description").toUpperCase(Locale.US) +
                                            "\n" + "Humidity: " + main.getString("humidity") + "%" +
                                            "\n" + "Pressure: " + main.getString("pressure") + " hPa");

                            txtCurrentTemperature.setText(
                                    String.format("%.2f", main.getDouble("temp")) + (char) 0x00B0 + "C");

                            DateFormat df = DateFormat.getDateTimeInstance();
                            String updatedOn = df.format(new Date(obj.getLong("dt") * 1000));
                            txtUpdated.setText("Last update: " + updatedOn);

                            setWeatherIcon(details.getInt("id"),
                                    obj.getJSONObject("sys").getLong("sunrise") * 1000,
                                    obj.getJSONObject("sys").getLong("sunset") * 1000);


                        } catch (JSONException e) {
                            Toast.makeText(getApplicationContext(), getString(R.string.place_not_found),
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                    }
                });
        ServiceHelper.getInstance().addToRequestQueue(stringRequest);
    }


    private void setWeatherIcon(int actualId, long sunrise, long sunset) {
        int id = actualId / 100;
        String icon = "";
        if (actualId == 800) {
            long currentTime = new Date().getTime();
            if (currentTime >= sunrise && currentTime < sunset) {
//                icon = getString(R.string.weather_sunny);
                weatherIcon.setImageResource(R.drawable.sunny);
            } else {
//                icon = getString(R.string.weather_clear_night);
                weatherIcon.setImageResource(R.drawable.clear_night);
            }
        } else {
            switch (id) {
                case 2:
                    weatherIcon.setImageResource(R.drawable.thunder);
//                    icon = getString(R.string.weather_thunder);
                    break;
                case 3:
                    weatherIcon.setImageResource(R.drawable.drizzle);
//                    icon = getString(R.string.weather_drizzle);
                    break;
                case 7:
                    weatherIcon.setImageResource(R.drawable.foggy);
//                    icon = getString(R.string.weather_foggy);
                    break;
                case 8:
                    weatherIcon.setImageResource(R.drawable.cloudy);
//                    icon = getString(R.string.weather_cloudy);
                    break;
                case 6:
                    weatherIcon.setImageResource(R.drawable.snow);
//                    icon = getString(R.string.weather_snowy);
                    break;
                case 5:
                    weatherIcon.setImageResource(R.drawable.rainy);
//                    icon = getString(R.string.weather_rainy);
                    break;
            }
        }
//        weatherIcon.setText(icon);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.change_city) {
            showInputDialog();
        } else if (id == R.id.menu_about) {
            about();
            return true;
        } else if (id == R.id.menu_exit) {
            closeApp();
            return true;
        } else if (id == R.id.refresh) {
            refresh();
            return true;
        } else if (id == R.id.my_location) {
            myLocation();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void showInputDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Change city");
        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);
        builder.setPositiveButton("Go", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                city = input.getText().toString();
                changeCity();
            }
        });
        builder.show();
    }

    public void changeCity() {
        id = 0;
        SharedPreferences.Editor editor = prefs.edit();

        editor.putString("city", city);
        editor.commit();

        currentWeather();
//        prefs.edit().putString("city", city).commit();
    }

    public void refresh() {
        id = 0;
        currentWeather();
    }

    public void about() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(getString(R.string.app_name));
        alertDialogBuilder.setMessage(getString(R.string.about_me));
        alertDialogBuilder.setPositiveButton(getString(R.string.ok), null);
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void closeApp() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(getString(R.string.exit));
        alertDialogBuilder.setMessage(getString(R.string.want_to_exit));
        alertDialogBuilder.setPositiveButton(getString(R.string.exit), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
                System.exit(0);
            }
        });
        alertDialogBuilder.setNegativeButton(getString(R.string.cancel), null);
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }


    public boolean isInternetAvailable(Context context) {
        ConnectivityManager con = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
        boolean wifi = con.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnectedOrConnecting();
        boolean internet = con.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).isConnectedOrConnecting();
        //check Internet connection
        if (internet || wifi) {
//            Toast.makeText(this, "Connected", Toast.LENGTH_SHORT).show();
            return true;
        } else {
//            Toast.makeText(this, "Not Connected", Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    public void myLocation() {
        LocationTraker traker = new LocationTraker(this);

        if (traker.getState()) {
            latitude = traker.getlat();
            longitude = traker.getLon();

        }
        id = 1;
        currentWeather();
    }

}
