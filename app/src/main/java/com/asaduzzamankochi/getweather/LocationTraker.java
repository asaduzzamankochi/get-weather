package com.asaduzzamankochi.getweather;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class LocationTraker implements LocationListener {
    private Context context;
    private LocationManager locationManager;
    private double latitude, longtude;
    private boolean locationState = false;
    private boolean gpsEnabled = false;
    private boolean NetworkEnabled = false;
    private boolean PassiveEnabled = false;
    private Location location;

    public LocationTraker(Context context) {
        this.context = context;

        getlocation();

    }

    private Location getlocation() {

        locationManager = (LocationManager) context
                .getSystemService(Context.LOCATION_SERVICE);

        gpsEnabled = locationManager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);
        NetworkEnabled = locationManager
                .isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        PassiveEnabled = locationManager
                .isProviderEnabled(LocationManager.PASSIVE_PROVIDER);

        if (gpsEnabled) {


            locationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, 0, 0, this);

            if (locationManager != null) {
                locationState = true;
                location = locationManager
                        .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                if (location != null) {
                    latitude = location.getLatitude();
                    longtude = location.getLongitude();

                }

            }

        } else if (NetworkEnabled) {

            locationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER, 0, 0, this);

            if (locationManager != null) {
                locationState = true;
                location = locationManager
                        .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if (location != null) {
                    latitude = location.getLatitude();
                    longtude = location.getLongitude();

                }

            }

        } else if (PassiveEnabled) {

            locationManager.requestLocationUpdates(
                    LocationManager.PASSIVE_PROVIDER, 0, 0, this);

            if (locationManager != null) {
                locationState = true;
                location = locationManager
                        .getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
                if (location != null) {
                    latitude = location.getLatitude();
                    longtude = location.getLongitude();

                }

            }

        }

        return location;
    }

    public double getlat() {
        if (location != null) {
            latitude = location.getLatitude();
        }

        return latitude;
    }

    public double getLon() {
        if (location != null) {
            longtude = location.getLongitude();
        }

        return longtude;
    }

    public boolean getState() {

        return locationState;
    }

    @Override
    public void onLocationChanged(Location location) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onProviderEnabled(String provider) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onProviderDisabled(String provider) {
        // TODO Auto-generated method stub

    }

    public String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        Geocoder geocoder;
        String address = null;
        String city = null;
        String country = null;
        String postalCode = null;
        String knownName = null;
        List<Address> addresses;
        geocoder = new Geocoder(context, Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);

            address = addresses.get(0).getAddressLine(0);
            city = addresses.get(0).getAddressLine(1);
            country = addresses.get(0).getAddressLine(2);

            postalCode = addresses.get(0).getAddressLine(3);
            knownName = addresses.get(0).getAdminArea();

            Log.i("TAG", "----" + address);
            Log.i("TAG", "---" + city);
            Log.i("TAG", "---" + country);

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

            Log.i("TAG", "----" + e.toString());
        }

        return address + ",\n" + city + ",\n" + country;
//        return "Address: " + address + ",\n" + "City: " + city + ",\n"
//                + "Country: " + country + ",\n" + "postalCode: " + postalCode + ",\n" + "knownName: " + knownName;
    }
}
